**Task 16 - Developer wants a computer!**

- A Developer wants to buy a new computer to write his code
- His bank balance is currently too low  
- Write a program where he can work for money 
    - Clicking the “Work for money” button should add 10.00 Kr to his bank balance… He’ll have to work hard
    - Clicking the “Get Loan From Bank” button should attempt to add enough funds to his bank account to buy the selected computer
    - He won’t be able to get a loan twice  
- If he has enough money and clicks “Buy a computer” he will get a nice message on the screen to Say he owns a new Computer
- If he tries to buy a computer without enough money, he should get a message that he was banned from the store  

<img src="../img/user-interface.png" alt="interface">
