// Global reference to Computer price.
// Changed value of option in select to price of computer.
const elComputerPrice = document.getElementById('computers');
elComputerPrice.addEventListener('change', function() {
    computerCost = this.value;
});

let bankBalance = 0.00;
let loanValue = 50;
let computerCost = parseInt(elComputerPrice.value);

let total = 0;
let maxNumberLoan = 1;
loansGiven = 0;

getMoney = () => {
   bankBalance += 10;

   document.getElementById("p1").innerHTML = `${bankBalance} Kr`;
}
 
getLoan = () => {
    if (loansGiven >= maxNumberLoan) {
        Swal.fire({
            icon: 'warning',
            title: 'Sorry',
            text: 'You have already received the maximum amount of loans!',
        })

        } else if (bankBalance < loanValue) {
        Swal.fire({
            icon: 'warning',
            title: 'Sorry',
            text: 'You can not get a loan!',
        }) 
        
    } else {
        sumLeft = computerCost - bankBalance;
        total = bankBalance + sumLeft;

        document.getElementById("p1").innerHTML = `${total} Kr`;
        bankBalance = total;
 
        Swal.fire({
            icon: 'success',
            title: 'Congrats',
            text: 'YOUR LOAN WAS ACCEPTED!',
        }) 
        loansGiven += 1;
    }
}

buyComputer = () => {
    if (bankBalance < computerCost) {  
        Swal.fire({
            icon: 'error',
            title: 'Warning!',
            text: 'You are banned from the store!',
        }) 
        
    } else {
        Swal.fire({
            icon: 'success',
            title: 'Congrats',
            text: 'You now have a new computer!',
        }) 
        
        document.getElementById("p1").innerHTML = `${bankBalance - computerCost} Kr`;
        bankBalance = 0;
    }
}
